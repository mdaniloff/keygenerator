﻿namespace KeyGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listProducts = new System.Windows.Forms.CheckedListBox();
            this.cboTimePeriod = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoLicensed = new System.Windows.Forms.RadioButton();
            this.rdoEval = new System.Windows.Forms.RadioButton();
            this.cmdGenerate = new System.Windows.Forms.Button();
            this.cmdExit = new System.Windows.Forms.Button();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listProducts
            // 
            this.listProducts.CheckOnClick = true;
            this.listProducts.FormattingEnabled = true;
            this.listProducts.Location = new System.Drawing.Point(21, 12);
            this.listProducts.Name = "listProducts";
            this.listProducts.Size = new System.Drawing.Size(429, 229);
            this.listProducts.TabIndex = 0;
            // 
            // cboTimePeriod
            // 
            this.cboTimePeriod.FormattingEnabled = true;
            this.cboTimePeriod.Location = new System.Drawing.Point(21, 356);
            this.cboTimePeriod.Name = "cboTimePeriod";
            this.cboTimePeriod.Size = new System.Drawing.Size(429, 21);
            this.cboTimePeriod.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoLicensed);
            this.groupBox1.Controls.Add(this.rdoEval);
            this.groupBox1.Location = new System.Drawing.Point(21, 247);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(429, 80);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // rdoLicensed
            // 
            this.rdoLicensed.AutoSize = true;
            this.rdoLicensed.Location = new System.Drawing.Point(287, 16);
            this.rdoLicensed.Name = "rdoLicensed";
            this.rdoLicensed.Size = new System.Drawing.Size(68, 17);
            this.rdoLicensed.TabIndex = 1;
            this.rdoLicensed.TabStop = true;
            this.rdoLicensed.Text = "Licensed";
            this.rdoLicensed.UseVisualStyleBackColor = true;
            this.rdoLicensed.CheckedChanged += new System.EventHandler(this.rdoLicensed_CheckedChanged);
            // 
            // rdoEval
            // 
            this.rdoEval.AutoSize = true;
            this.rdoEval.Location = new System.Drawing.Point(75, 16);
            this.rdoEval.Name = "rdoEval";
            this.rdoEval.Size = new System.Drawing.Size(75, 17);
            this.rdoEval.TabIndex = 0;
            this.rdoEval.TabStop = true;
            this.rdoEval.Text = "Evaluation";
            this.rdoEval.UseVisualStyleBackColor = true;
            this.rdoEval.CheckedChanged += new System.EventHandler(this.rdoEval_CheckedChanged);
            // 
            // cmdGenerate
            // 
            this.cmdGenerate.Location = new System.Drawing.Point(286, 624);
            this.cmdGenerate.Name = "cmdGenerate";
            this.cmdGenerate.Size = new System.Drawing.Size(75, 23);
            this.cmdGenerate.TabIndex = 3;
            this.cmdGenerate.Text = "Generate";
            this.cmdGenerate.UseVisualStyleBackColor = true;
            this.cmdGenerate.Click += new System.EventHandler(this.cmdGenerate_Click);
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(375, 624);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(75, 23);
            this.cmdExit.TabIndex = 4;
            this.cmdExit.Text = "Exit";
            this.cmdExit.UseVisualStyleBackColor = true;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // txtDisplay
            // 
            this.txtDisplay.Location = new System.Drawing.Point(21, 406);
            this.txtDisplay.Multiline = true;
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.Size = new System.Drawing.Size(429, 200);
            this.txtDisplay.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 659);
            this.Controls.Add(this.txtDisplay);
            this.Controls.Add(this.cmdExit);
            this.Controls.Add(this.cmdGenerate);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cboTimePeriod);
            this.Controls.Add(this.listProducts);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox listProducts;
        private System.Windows.Forms.ComboBox cboTimePeriod;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoLicensed;
        private System.Windows.Forms.RadioButton rdoEval;
        private System.Windows.Forms.Button cmdGenerate;
        private System.Windows.Forms.Button cmdExit;
        private System.Windows.Forms.TextBox txtDisplay;
    }
}

