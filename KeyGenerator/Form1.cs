﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KeyGenerator
{
    public partial class Form1 : Form
    {
        private UInt16 _good_crc;
        private const UInt16 poly = 0x1021;
        private bool _LicensedCopy = false;
        StringBuilder _textBuffer = new StringBuilder(1024);
        private int _timeTerm;
        private int _displayTerm;
        // in days (for display)
        private int _timePeriod;

        private string _expirationDate = String.Empty;
        public Form1()
        {
            InitializeComponent();
            LoadData();
        }


        private void LoadData()
        {
            LoadProducts();
            rdoEval.Checked = true;
        }

        private void LoadProducts()
        {
            listProducts.Items.Add(ProductDisplay.ISCRUB);
            //listProducts.Items.Add(ProductDisplay.ISCRUB_NO_PDF);
            listProducts.Items.Add(ProductDisplay.IREDLINE);
            listProducts.Items.Add(ProductDisplay.ICREATE);
            listProducts.Items.Add(ProductDisplay.IDOCID);
            listProducts.Items.Add(ProductDisplay.IREDLINE_LITE);
            listProducts.Items.Add(ProductDisplay.IDISCOVER);
            listProducts.Items.Add(ProductDisplay.IHYPERSTYLES);
            listProducts.Items.Add(ProductDisplay.ICALENDAR);
            listProducts.Items.Add(ProductDisplay.IBATESLABEL);
        }


        private void cmdExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdGenerate_Click(object sender, EventArgs e)
        {
           if (listProducts.CheckedItems.Count == 0)
                return;

            string s = "";
            //for (int x = 0; x <= listProducts.CheckedItems.Count - 1; x++)
            //{
            //    s = s + "Checked Item " + (x + 1).ToString() + " = " + listProducts.CheckedItems[x].ToString() + "\n";
            //}
            var lstOfProductIndexes = new List<int>();
            for (int x = 0; x <= listProducts.CheckedIndices.Count - 1; x++)
            {
                s = s + "Checked Item " + (x + 1).ToString() + " = " + listProducts.CheckedIndices[x].ToString() + "\n";
                lstOfProductIndexes.Add(listProducts.CheckedIndices[x]);
            }
            //MessageBox.Show(s);

            _LicensedCopy = rdoLicensed.Checked;

            GenerateLicenses(lstOfProductIndexes);
        }

        private void GenerateLicenses(IEnumerable<int> indexes)
        {
            _textBuffer.Clear();
            _textBuffer.Append("The following product(s) licensed for : ");
            _textBuffer.Append("\r\n");
            foreach (var index in indexes)
            {
                GenerateLicenseKey(index + 1);
            }

            string timeNotice = BuildTimeNotice();
            _textBuffer.Append("\r\n");
            _textBuffer.Append(timeNotice);

            _textBuffer.Append("\r\n");
            _textBuffer.Append("\r\n");
            string expiryNote = BuildExpiryNote();
            _textBuffer.Append(expiryNote);
            txtDisplay.Text = _textBuffer.ToString();

            System.Windows.Forms.Clipboard.SetText(txtDisplay.Text);
        }


        private void GenerateLicenseKey(int index)
        {
            string productCode = GetProductCodeQuadrant(index);

            string timeCode = GetTimePeriodQuadrant();

            string szKey = timeCode + productCode;

            string szFirstCrc = string.Empty;

            EncryptString(szKey, ref szFirstCrc);

            szKey += szFirstCrc;

            string szSecondCrc = string.Empty;

            EncryptString(szKey, ref szSecondCrc);

            szKey = timeCode + "-" + productCode + "-" + szFirstCrc + "-" + szSecondCrc;

            string productName = GetProductName(index);
            _textBuffer.Append("\r\n");
            _textBuffer.Append(productName + " - the serial number is :" + szKey);
            _textBuffer.Append("\r\n");
        }

        private string GetProductCodeQuadrant(int code)
        {
            var rnd = new Random();

            int num = rnd.Next(1, 99);

            int coded = num + code + 100;

            string numHi = num.ToString("X2");
            string numLo = coded.ToString("X2");
            return numHi + numLo;
        }

        private string GetTimePeriodQuadrant()
        {
            _timeTerm = GetTimeTerm();
            ushort term = (ushort)_timeTerm;
            //DateTime dt = new DateTime(2016, 2, 23, 0, 0, 0, 0);
            DateTime dt = GetStartDateTime();
            long days = ConvertToTimestamp(dt);

            const long baseDate = 1102380000L;

            ushort ndays = (ushort)((days - baseDate) / 86400L);

            string encodedDays = ndays.ToString("X4");

           

            ushort result = (ushort) (term | ndays);

            return result.ToString("X4");
        }

        // WORKAROUND to take care of the time bomb 02-23-2016
        private DateTime GetStartDateTime()
        {
            if (_timePeriod > 0)
            {
                TimeSpan span = new TimeSpan(_timePeriod, 0, 0, 0, 0);
                DateTime temp = DateTime.Now + span;
                _expirationDate = temp.ToShortDateString();
                int years = (_timePeriod / 365) + 2;

                DateTime start = temp - new TimeSpan(years*365, 0, 0, 0, 0);

                return start;
            }
            else
            {
                return DateTime.Now - new TimeSpan(365, 0, 0, 0, 0);

            }
        }
       
        private int GetTimeTerm()
        {
            int selectedTermIndex = cboTimePeriod.SelectedIndex;
            int span = 0;
            if (rdoEval.Checked)
            {
                switch (selectedTermIndex)
                {
                    case 0:
                        _displayTerm = TimeTerm.EVAL_30_DAYS;
                        span = TimeTerm.REGISTERED_2_YEAR;
                        _timePeriod = 30;
                        break;
                    case 1:
                        _displayTerm = TimeTerm.EVAL_60_DAYS;
                        span = TimeTerm.REGISTERED_2_YEAR;
                        _timePeriod = 60;
                        break;
                    case 2:
                        _displayTerm = TimeTerm.EVAL_90_DAYS;
                        span = TimeTerm.REGISTERED_2_YEAR;
                        _timePeriod = 90;
                        break;

                    case 3:
                        _displayTerm = TimeTerm.EVAL_180_DAYS;
                        span = TimeTerm.REGISTERED_2_YEAR;
                        _timePeriod = 180;
                        break;

                    case 4:
                        _displayTerm = TimeTerm.EVAL_7_DAYS;
                        span = TimeTerm.REGISTERED_2_YEAR;
                        _timePeriod = 7;
                        break;

                    case 5:
                        _displayTerm = TimeTerm.EVAL_14_DAYS;
                        span = TimeTerm.REGISTERED_2_YEAR;
                        _timePeriod = 14;
                        break;
                }
            }
            else
            {
                switch (selectedTermIndex)
                {
                    case 0:
                        _displayTerm = TimeTerm.REGISTERED_1_YEAR;
                        span = TimeTerm.REGISTERED_3_YEAR;
                        _timePeriod = 366;
                        break;
                    case 1:
                        _displayTerm = TimeTerm.REGISTERED_2_YEAR;
                        span = TimeTerm.REGISTERED_3_YEAR;
                        _timePeriod = 731;
                        break;
                    case 2:
                        _displayTerm = TimeTerm.REGISTERED_3_YEAR;
                        span = TimeTerm.REGISTERED_3_YEAR;
                        _timePeriod = 1096;
                        break;

                    case 3:
                        _displayTerm = TimeTerm.REGISTERED_NO_EXPIRE;
                        span = TimeTerm.REGISTERED_NO_EXPIRE;
                        _timePeriod = -1;
                        break;
                }
            }


            return span;

        }


        private string BuildTimeNotice()
        {
            switch (_displayTerm)
            {
                case TimeTerm.EVAL_30_DAYS:
                    return "Evaluation time is 30 days";
                case TimeTerm.EVAL_60_DAYS:
                    return "Evaluation time is 60 days";
                case TimeTerm.EVAL_90_DAYS:
                    return "Evaluation time is 90 days";
                case TimeTerm.EVAL_180_DAYS:
                    return "Evaluation time is 180 days";
                case TimeTerm.EVAL_7_DAYS:
                    return "Evaluation time is 7 days";
                case TimeTerm.EVAL_14_DAYS:
                    return "Evaluation time is 14 days";
                case TimeTerm.REGISTERED_1_YEAR:
                    return "Product(s) licensed for 1 Year";
                case TimeTerm.REGISTERED_2_YEAR:
                    return "Product(s) licensed for 2 Years";
                case TimeTerm.REGISTERED_3_YEAR:
                    return "Product(s) licensed for 3 Years";
                case TimeTerm.REGISTERED_NO_EXPIRE:
                    return "Product(s) licensed for: no expiration date";
                default:
                    return String.Empty;
            }
        }

        private string BuildExpiryNote()
        {
            if (_timePeriod < 0)
                return String.Empty;

            //throw new Exception("Not Impletemented yet");
            return "The license expires on " + _expirationDate;

        }

        private long ConvertToTimestamp(DateTime value)
        {
            //create Timespan by subtracting the value provided from
            //the Unix Epoch
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());

            //return the total seconds (which is a UNIX timestamp)
            return (long)span.TotalSeconds;
        }

        private void rdoLicensed_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;
            if (button != null && button.Checked)
            {
                cboTimePeriod.Items.Clear();
                cboTimePeriod.Items.Add("1 Year");
                cboTimePeriod.Items.Add("2 Years");
                cboTimePeriod.Items.Add("3 Years");
                cboTimePeriod.Items.Add("No Expiration");
                cboTimePeriod.SelectedIndex = 0;
            }
        }

        private void rdoEval_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;
            if (button != null && button.Checked)
            {
                cboTimePeriod.Items.Clear();
                cboTimePeriod.Items.Add("30 Days");
                cboTimePeriod.Items.Add("60 Days");
                cboTimePeriod.Items.Add("90 Days");
                cboTimePeriod.Items.Add("180 Days");
                cboTimePeriod.Items.Add("7 Days");
                cboTimePeriod.Items.Add("14 Days");

                cboTimePeriod.SelectedIndex = 0;
            }
        }

        private string GetProductName(int productId)
        {
            switch (productId)
            {
                case ProductId.IScrub:
                    return ProductDisplay.ISCRUB;
                case ProductId.IScrubNoPdf:
                    return ProductDisplay.ISCRUB_NO_PDF;
                case ProductId.ICreate:
                    return ProductDisplay.ICREATE;
                case ProductId.IDocid:
                    return ProductDisplay.IDOCID;
                case ProductId.IHyprestyles:
                    return ProductDisplay.IHYPERSTYLES;
                case ProductId.IRedline:
                    return ProductDisplay.IREDLINE;
                case ProductId.IRedlineLite:
                    return ProductDisplay.IREDLINE_LITE;
                case ProductId.IBatesLabel:
                    return ProductDisplay.IBATESLABEL;
                case ProductId.ICalendar:
                    return ProductDisplay.ICALENDAR;
                case ProductId.IDiscover:
                    return ProductDisplay.IDISCOVER;
                default:
                    return String.Empty;

            }
        }

        void EncryptString(string source, ref string srcCRC)
        {
            ushort text_length;
            ushort ch, i;

            _good_crc = 0xFFFF;
            i = 0;
            text_length = 0;
            while (i < source.Length)
            {
                ch = source[i];
                update_good_crc(ch);
                i++;
                text_length++;
            }
            augment_message_for_good_crc();
            srcCRC = _good_crc.ToString("X4");
        }

     

        void update_good_crc(ushort ch)
        {
            ushort i, v, xor_flag;

            v = 0x80;

            for (i = 0; i < 8; i++)
            {
                int result = _good_crc & 0x8000;

                if (result != 0)
                {
                    xor_flag = 1;
                }
                else
                {
                    xor_flag = 0;
                }
                _good_crc = (ushort)(_good_crc << 1);

                int res = ch & v;

                if (res != 0)
                {
                    /*
                    Append next bit of message to end of CRC if it is not zero.
                    The zero bit placed there by the shift above need not be
                    changed if the next bit of the message is zero.
                    */
                    _good_crc = (ushort)(_good_crc + 1);
                }

                if (xor_flag != 0)
                {
                    _good_crc = (ushort)(_good_crc ^ poly);
                }

                /*
                Align test bit with next bit of the message byte.
                */
                v = (ushort)(v >> 1);
            }
        }

        void augment_message_for_good_crc()
        {
            ushort i, xor_flag;

            for (i = 0; i < 16; i++)
            {
                int result = _good_crc & 0x8000;
                if (result != 0)
                {
                    xor_flag = 1;
                }
                else
                {
                    xor_flag = 0;
                }
                _good_crc = (ushort)(_good_crc << 1);

                if (xor_flag != 0)
                {
                    _good_crc = (ushort)(_good_crc ^ poly);
                }
            }
        }
    }
}
