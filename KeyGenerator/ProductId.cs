﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeyGenerator
{
    internal static class ProductId
    {
        internal const   int IScrub = 1;
        internal const int IRedline = 2;
        internal const int ICreate = 3;
        internal const int IDocid = 4;
        internal const int IRedlineLite = 5;
        internal const int IDiscover = 6;
        internal const int IHyprestyles = 7;
        internal const int ICalendar = 8;
        internal const int IBatesLabel = 9;
        internal const int IScrubNoPdf = 10;
    }

    internal static class ProductDisplay
    {
        internal const string ISCRUB = "iScrub";
        internal const string ISCRUB_NO_PDF = "iScrub without PDF";
        internal const string IREDLINE = "iRedline";
        internal const string ICREATE = "iCreate";
        internal const string IDOCID = "iDocID";
        internal const string IREDLINE_LITE = "iRedlineLite";
        internal const string IDISCOVER = "iDiscover";
        internal const string IHYPERSTYLES = "iHyperstyles";
        internal const string ICALENDAR = "iCalendar";
        internal const string IBATESLABEL = "iBatesLabel";
    }

    internal static class TimeTerm
    {
        internal const int EVAL_30_DAYS = 0x1000;

        internal const int EVAL_60_DAYS = 0x2000;

        internal const int EVAL_90_DAYS = 0x3000;

        internal const int EVAL_180_DAYS = 0x4000;

        internal const int EVAL_7_DAYS = 0x5000;

        internal const int EVAL_14_DAYS = 0x6000;

        internal const int REGISTERED_1_YEAR = 0x9000;
        internal const int REGISTERED_2_YEAR = 0xA000;
        internal const int REGISTERED_3_YEAR = 0xB000;
        internal const int REGISTERED_NO_EXPIRE = 0xC000;
    }
}
